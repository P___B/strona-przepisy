package com.patryk_baranowski_Z613.Projekt.Spring.controller;

import com.patryk_baranowski_Z613.Projekt.Spring.model.RecipesModel;
import com.patryk_baranowski_Z613.Projekt.Spring.repo.RecipesRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/recipes")
public class RecipesController {
    @Autowired
    private RecipesRepo recipesRepo;

    @GetMapping
    public List<RecipesModel> findAll() {
        return recipesRepo.findAll();
    }

    @PostMapping
    public RecipesModel save(@RequestBody RecipesModel recipesModel) {
        return recipesRepo.save(recipesModel);
    }

    @PutMapping
    public RecipesModel update(@RequestBody RecipesModel recipesModel) {
        return recipesRepo.save(recipesModel);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable Long id) {
        recipesRepo.deleteById(id);
    }
}
