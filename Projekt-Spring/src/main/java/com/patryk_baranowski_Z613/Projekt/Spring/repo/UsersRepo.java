package com.patryk_baranowski_Z613.Projekt.Spring.repo;

import com.patryk_baranowski_Z613.Projekt.Spring.model.UsersModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepo extends JpaRepository<UsersModel, Long> {
}
