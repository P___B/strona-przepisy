package com.patryk_baranowski_Z613.Projekt.Spring.repo;

import com.patryk_baranowski_Z613.Projekt.Spring.model.RecipesModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecipesRepo extends JpaRepository<RecipesModel, Long> {
}
