package com.patryk_baranowski_Z613.Projekt.Spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjektSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjektSpringApplication.class, args);
	}

}
