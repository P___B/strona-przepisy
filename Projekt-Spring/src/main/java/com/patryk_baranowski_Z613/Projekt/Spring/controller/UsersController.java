package com.patryk_baranowski_Z613.Projekt.Spring.controller;

import com.patryk_baranowski_Z613.Projekt.Spring.model.UsersModel;
import com.patryk_baranowski_Z613.Projekt.Spring.repo.UsersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/users")
public class UsersController {
    @Autowired
    private UsersRepo usersRepo;

    @GetMapping
    public List<UsersModel> findAll() {
        return usersRepo.findAll();
    }

    @GetMapping(value = "/{id}")
    public Optional<UsersModel> findById(@PathVariable Long id) { return usersRepo.findById(id); }

    @PostMapping
    public UsersModel save(@RequestBody UsersModel usersModel) {
        return usersRepo.save(usersModel);
    }

    @PutMapping
    public UsersModel update(@RequestBody UsersModel usersModel) {
        return usersRepo.save(usersModel);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable Long id) { usersRepo.deleteById(id); }
}
