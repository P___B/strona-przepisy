package com.patryk_baranowski_Z613.Projekt.Spring.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.lang.String;
import java.lang.String;

@Entity
public class RecipesModel {
    private Long id;
    private String name;
    private String type;
    private String hardness;
    private String ingredients;
    private String time;
    private String description;
    private String image;
    private String author;
    private String comments;
    private Boolean deleted;

    public RecipesModel() {
    }

    public RecipesModel(Long id, String name, String type, String hardness, String ingredients, String time, String description, String image, String author, String comments, Boolean deleted) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.hardness = hardness;
        this.ingredients = ingredients;
        this.time = time;
        this.description = description;
        this.image = image;
        this.author = author;
        this.comments = comments;
        this.deleted = deleted;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHardness() {
        return hardness;
    }

    public void setHardness(String hardness) {
        this.hardness = hardness;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
