'use strict';

define('frontend/tests/app.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | app');

  QUnit.test('app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'app.js should pass ESLint\n\n');
  });

  QUnit.test('components/add-new-recipe.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'components/add-new-recipe.js should pass ESLint\n\n3:16 - Use import Component from \'@ember/component\'; instead of using Ember.Component (ember/new-module-imports)\n4:12 - Use import { inject } from \'@ember/service\'; instead of using Ember.inject.service (ember/new-module-imports)\n5:18 - Use import { inject } from \'@ember/service\'; instead of using Ember.inject.service (ember/new-module-imports)\n8:3 - Only string, number, symbol, boolean, null, undefined, and function are allowed as default properties (ember/avoid-leaking-state-in-ember-objects)\n10:3 - Only string, number, symbol, boolean, null, undefined, and function are allowed as default properties (ember/avoid-leaking-state-in-ember-objects)\n18:25 - Don\'t use .on() for component lifecycle events. (ember/no-on-calls-in-components)\n18:25 - Use import { on } from \'@ember/object/evented\'; instead of using Ember.on (ember/new-module-imports)\n18:42 - Use import { observer } from \'@ember/object\'; instead of using Ember.observer (ember/new-module-imports)\n19:5 - Use import { scheduleOnce } from \'@ember/runloop\'; instead of using Ember.run.scheduleOnce (ember/new-module-imports)\n20:26 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n35:9 - Do not use global `$` or `jQuery` (ember/no-global-jquery)\n35:9 - \'$\' is not defined. (no-undef)\n53:7 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)\n56:7 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)\n59:18 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n60:18 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n61:22 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n62:25 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n63:18 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n64:25 - Do not use global `$` or `jQuery` (ember/no-global-jquery)\n64:25 - \'$\' is not defined. (no-undef)\n65:19 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n66:21 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n68:26 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)');
  });

  QUnit.test('components/login-modal.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'components/login-modal.js should pass ESLint\n\n3:16 - Use import Component from \'@ember/component\'; instead of using Ember.Component (ember/new-module-imports)\n4:12 - Use import { inject } from \'@ember/service\'; instead of using Ember.inject.service (ember/new-module-imports)\n5:18 - Use import { inject } from \'@ember/service\'; instead of using Ember.inject.service (ember/new-module-imports)\n11:21 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n12:22 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n13:22 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n24:11 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)');
  });

  QUnit.test('components/register-modal.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'components/register-modal.js should pass ESLint\n\n3:16 - Use import Component from \'@ember/component\'; instead of using Ember.Component (ember/new-module-imports)\n4:12 - Use import { inject } from \'@ember/service\'; instead of using Ember.inject.service (ember/new-module-imports)\n5:18 - Use import { inject } from \'@ember/service\'; instead of using Ember.inject.service (ember/new-module-imports)\n12:21 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n13:22 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n14:22 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n15:18 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)');
  });

  QUnit.test('controllers/application.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'controllers/application.js should pass ESLint\n\n4:16 - Use import Controller from \'@ember/controller\'; instead of using Ember.Controller (ember/new-module-imports)\n8:12 - Use import { inject } from \'@ember/service\'; instead of using Ember.inject.service (ember/new-module-imports)\n10:18 - Use import { inject } from \'@ember/service\'; instead of using Ember.inject.service (ember/new-module-imports)\n14:23 - Use import { observer } from \'@ember/object\'; instead of using Ember.observer (ember/new-module-imports)\n25:20 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n29:23 - Use import { computed } from \'@ember/object\'; instead of using Ember.computed (ember/new-module-imports)\n40:3 - Only string, number, symbol, boolean, null, undefined, and function are allowed as default properties (ember/avoid-leaking-state-in-ember-objects)\n46:3 - Only string, number, symbol, boolean, null, undefined, and function are allowed as default properties (ember/avoid-leaking-state-in-ember-objects)\n47:3 - Only string, number, symbol, boolean, null, undefined, and function are allowed as default properties (ember/avoid-leaking-state-in-ember-objects)\n49:19 - Use import { observer } from \'@ember/object\'; instead of using Ember.observer (ember/new-module-imports)\n51:5 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)\n53:19 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n59:7 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)\n62:7 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)\n72:7 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)\n76:7 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)\n79:21 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n86:9 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)\n87:9 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)\n98:7 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)\n101:7 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)\n103:21 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)');
  });

  QUnit.test('models/recipes.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/recipes.js should pass ESLint\n\n');
  });

  QUnit.test('resolver.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'resolver.js should pass ESLint\n\n');
  });

  QUnit.test('router.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'router.js should pass ESLint\n\n');
  });

  QUnit.test('routes/application.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'routes/application.js should pass ESLint\n\n2:8 - \'RSVP\' is defined but never used. (no-unused-vars)\n4:16 - Use import Route from \'@ember/routing/route\'; instead of using Ember.Route (ember/new-module-imports)\n5:12 - Use import { inject } from \'@ember/service\'; instead of using Ember.inject.service (ember/new-module-imports)\n8:19 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n33:7 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)\n48:7 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)\n53:41 - \'model\' is defined but never used. (no-unused-vars)');
  });

  QUnit.test('services/data-api.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'services/data-api.js should pass ESLint\n\n3:16 - Use import Service from \'@ember/service\'; instead of using Ember.Service (ember/new-module-imports)\n4:18 - Use import { inject } from \'@ember/service\'; instead of using Ember.inject.service (ember/new-module-imports)\n5:3 - Only string, number, symbol, boolean, null, undefined, and function are allowed as default properties (ember/avoid-leaking-state-in-ember-objects)\n6:3 - Only string, number, symbol, boolean, null, undefined, and function are allowed as default properties (ember/avoid-leaking-state-in-ember-objects)\n9:19 - Use import { computed } from \'@ember/object\'; instead of using Ember.computed (ember/new-module-imports)\n10:19 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n22:23 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n25:25 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n34:70 - \'res\' is defined but never used. (no-unused-vars)\n35:27 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n37:9 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)\n40:11 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)\n47:9 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)\n53:27 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n54:9 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)\n56:9 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)\n59:11 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)\n66:9 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)\n74:25 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n76:83 - \'res\' is defined but never used. (no-unused-vars)\n77:25 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n79:7 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)\n87:25 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n89:23 - Use import { get } from \'@ember/object\'; instead of using Ember.get (ember/new-module-imports)\n90:7 - Use import { set } from \'@ember/object\'; instead of using Ember.set (ember/new-module-imports)');
  });
});
define('frontend/tests/test-helper', ['frontend/app', 'frontend/config/environment', '@ember/test-helpers', 'ember-qunit'], function (_app, _environment, _testHelpers, _emberQunit) {
  'use strict';

  (0, _testHelpers.setApplication)(_app.default.create(_environment.default.APP));

  (0, _emberQunit.start)();
});
define('frontend/tests/tests.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | tests');

  QUnit.test('test-helper.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'test-helper.js should pass ESLint\n\n');
  });
});
define('frontend/config/environment', [], function() {
  var prefix = 'frontend';
try {
  var metaName = prefix + '/config/environment';
  var rawConfig = document.querySelector('meta[name="' + metaName + '"]').getAttribute('content');
  var config = JSON.parse(unescape(rawConfig));

  var exports = { 'default': config };

  Object.defineProperty(exports, '__esModule', { value: true });

  return exports;
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

});

require('frontend/tests/test-helper');
EmberENV.TESTS_FILE_LOADED = true;
//# sourceMappingURL=tests.map
