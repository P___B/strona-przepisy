"use strict";



;define('frontend/app', ['exports', 'frontend/resolver', 'ember-load-initializers', 'frontend/config/environment'], function (exports, _resolver, _emberLoadInitializers, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  const App = Ember.Application.extend({
    modulePrefix: _environment.default.modulePrefix,
    podModulePrefix: _environment.default.podModulePrefix,
    Resolver: _resolver.default
  });

  (0, _emberLoadInitializers.default)(App, _environment.default.modulePrefix);

  exports.default = App;
});
;define('frontend/components/add-new-recipe', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Component.extend({
    dataApi: Ember.inject.service('data-api'),
    notifications: Ember.inject.service('toast'),
    name: "",
    type: "Zupa",
    types: ["Zupa", "Drugie danie", "Śniadanie", "Kolacja"],
    hardness: "Łatwe",
    hardnessTypes: ["Łatwe", "Średnie", "Trudne"],
    ingredients: "",
    time: "",
    description: "",
    image: "",
    author: "",
    recipeToEdit: null,

    recipeToEditObserver: Ember.on('init', Ember.observer('recipeToEdit', function () {
      Ember.run.scheduleOnce('afterRender', this, function () {
        let recipeToEdit = Ember.get(this, 'recipeToEdit');

        if (recipeToEdit) {
          let {
            name,
            type,
            hardness,
            ingredients,
            time,
            description,
            image,
            author
          } = recipeToEdit;

          ingredients = ingredients.join(',');
          $('.textarea')[0].value = description;

          this.setProperties({
            name,
            type,
            hardness,
            ingredients,
            time,
            description,
            image,
            author
          });
        }
      });
    })),

    actions: {
      changeType(value) {
        Ember.set(this, "type", value);
      },
      changeHardness(value) {
        Ember.set(this, "hardness", value);
      },
      save() {
        let name = Ember.get(this, 'name');
        let type = Ember.get(this, 'type');
        let hardness = Ember.get(this, 'hardness');
        let ingredients = Ember.get(this, 'ingredients');
        let time = Ember.get(this, 'time');
        let description = $('.textarea')[0].value;
        let image = Ember.get(this, 'image');
        let dataApi = Ember.get(this, 'dataApi');
        let author = dataApi.get("whoami.username");
        let recipeToEdit = Ember.get(this, 'recipeToEdit');

        if (name.length == 0 || type.length == 0 || hardness.length == 0 || ingredients.length == 0 || time.length == 0 || description.length == 0 || image.length == 0 || author.length == 0) {
          this.get('notifications').error("Wszystkie pola są wymagane");
        } else {
          let recipe = {
            name,
            type,
            hardness,
            ingredients,
            time,
            description,
            image,
            author,
            comments: recipeToEdit ? recipeToEdit.comments.join('&') : "",
            id: recipeToEdit ? recipeToEdit.id : null
          };

          dataApi.updateRecipe(recipe);

          this.toggleProperty('showNewRecipeModal');

          this.setProperties({
            name: "",
            type: "",
            hardness: "",
            ingredients: "",
            time: "",
            description: "",
            image: "",
            author: ""
          });
        }
      },
      cancel() {
        this.toggleProperty('showNewRecipeModal');
        this.setProperties({
          name: "",
          type: "",
          hardness: "",
          ingredients: "",
          time: "",
          description: "",
          image: "",
          author: ""
        });
      }
    }
  });
});
;define('frontend/components/login-modal', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Component.extend({
    dataApi: Ember.inject.service('data-api'),
    notifications: Ember.inject.service('toast'),
    username: "Admin",
    password: "Admin",

    actions: {
      save() {
        let dataApi = Ember.get(this, 'dataApi');
        let username = Ember.get(this, 'username');
        let password = Ember.get(this, 'password');

        if (username.length == 0 || password.length == 0) {
          this.get('notifications').error("Wszystkie pola są wymagane");
        } else {
          let usersData = dataApi.get("usersData");

          let user = usersData.find(u => u.username == username);

          if (user && user.password == password) {
            Ember.set(dataApi, 'whoami', user);

            this.toggleProperty('showLoginModal');

            this.setProperties({
              username: "",
              password: ""
            });
          } else {
            this.get('notifications').error("Nie ma takiego użytkownika lub dane są nieprawidłowe");
          }
        }
      },
      cancel() {
        this.toggleProperty('showLoginModal');
        this.setProperties({
          username: "",
          password: ""
        });
      }
    }
  });
});
;define('frontend/components/register-modal', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Component.extend({
    dataApi: Ember.inject.service('data-api'),
    notifications: Ember.inject.service('toast'),
    username: "",
    password: "",
    role: "user",

    actions: {
      save() {
        let dataApi = Ember.get(this, 'dataApi');
        let username = Ember.get(this, 'username');
        let password = Ember.get(this, 'password');
        let role = Ember.get(this, 'role');

        if (username.length == 0 || password.length == 0 || role.length == 0) {
          this.get('notifications').error("Wszystkie pola są wymagane");
        } else {
          let user = {
            username,
            password,
            role
          };

          dataApi.saveUser(user);

          this.toggleProperty('showRegisterModal');

          this.setProperties({
            username: "",
            password: ""
          });
        }
      },
      cancel() {
        this.toggleProperty('showRegisterModal');
        this.setProperties({
          username: "",
          password: ""
        });
      }
    }
  });
});
;define('frontend/components/welcome-page', ['exports', 'ember-welcome-page/components/welcome-page'], function (exports, _welcomePage) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _welcomePage.default;
    }
  });
});
;define('frontend/controllers/application', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Controller.extend({
    showNewRecipeModal: false,
    showLoginModal: false,
    showRegisterModal: false,
    dataApi: Ember.inject.service('data-api'),
    recipeToEdit: null,
    notifications: Ember.inject.service('toast'),
    filterType: "Wszystko",
    filterOption: null,

    filterTypeObserver: Ember.observer('filterType', function () {
      if (this.get("filterType") == "Wszystko") {
        this.set('filterOption', null);
      } else if (this.get("filterType") == "Rodzaj") {
        this.set('filterOption', "Zupa");
      } else if (this.get("filterType") == "Trudność") {
        this.set('filterOption', 'Łatwe');
      } else if (this.get("filterType") == "Autor") {
        let users = Ember.get(this, 'dataApi.usersData');
        this.set('filterOption', users[0].username);
      }
    }),
    filterTypeProperty: Ember.computed('filterType', function () {
      if (this.get("filterType") == "Rodzaj") {
        return "type";
      } else if (this.get("filterType") == "Trudność") {
        return "hardness";
      } else if (this.get("filterType") == "Autor") {
        return 'author';
      }
    }),
    filterTypes: ["Wszystko", "Rodzaj", 'Trudność', 'Autor'],
    recipeTypes: ["Zupa", "Drugie danie", "Śniadanie", "Kolacja"],
    hardnessTypes: ["Łatwe", "Średnie", "Trudne"],

    whoamiObserver: Ember.observer("dataApi.whoami", function () {
      // Ember.set(this, 'filterOption', null);
      Ember.set(this, 'filterType', "Wszystko");
      // this.notifyPropertyChange('filterType');
      let dataApi = Ember.get(this, 'dataApi');
      dataApi.notifyPropertyChange('recipesData');
    }),

    actions: {
      logout() {
        Ember.set(this, "dataApi.whoami", null);
      },
      pickRecipe(recipe) {
        Ember.set(this, "dataApi.selectedRecipe", recipe);
      },
      loginModal() {
        this.toggleProperty("showLoginModal");
      },
      registerModal() {
        this.toggleProperty("showRegisterModal");
      },
      addNewRecipe() {
        this.toggleProperty("showNewRecipeModal");
        Ember.set(this, "recipeToEdit", null);
      },
      editRecipe(recipe) {
        this.toggleProperty("showNewRecipeModal");
        Ember.set(this, "recipeToEdit", recipe);
      },
      deleteRecipe(recipe) {
        let dataApi = Ember.get(this, 'dataApi');
        dataApi.deleteRecipe(recipe);
      },
      addNewComment(recipe) {
        if (recipe.comments.length < 5) {
          recipe.comments.pushObject(this.get("newCommentValue"));

          Ember.set(recipe, "ingredients", recipe.ingredients.join(','));
          Ember.set(recipe, "comments", recipe.comments.join('&'));

          this.get('dataApi').updateRecipe(recipe);

          this.set("newCommentValue", "");
        } else {
          this.get('notifications').error("Osiągnięto maksymalną liczbę komentarzy");
        }
      },
      pickFilterType(value) {
        Ember.set(this, "filterType", value);
      },
      pickFilterOption(value) {
        Ember.set(this, "filterOption", value);

        let dataApi = Ember.get(this, 'dataApi');
        dataApi.notifyPropertyChange('recipesData');
      }
    }
  });
});
;define('frontend/helpers/and', ['exports', 'ember-truth-helpers/helpers/and'], function (exports, _and) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _and.default;
    }
  });
  Object.defineProperty(exports, 'and', {
    enumerable: true,
    get: function () {
      return _and.and;
    }
  });
});
;define('frontend/helpers/app-version', ['exports', 'frontend/config/environment', 'ember-cli-app-version/utils/regexp'], function (exports, _environment, _regexp) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.appVersion = appVersion;
  function appVersion(_, hash = {}) {
    const version = _environment.default.APP.version;
    // e.g. 1.0.0-alpha.1+4jds75hf

    // Allow use of 'hideSha' and 'hideVersion' For backwards compatibility
    let versionOnly = hash.versionOnly || hash.hideSha;
    let shaOnly = hash.shaOnly || hash.hideVersion;

    let match = null;

    if (versionOnly) {
      if (hash.showExtended) {
        match = version.match(_regexp.versionExtendedRegExp); // 1.0.0-alpha.1
      }
      // Fallback to just version
      if (!match) {
        match = version.match(_regexp.versionRegExp); // 1.0.0
      }
    }

    if (shaOnly) {
      match = version.match(_regexp.shaRegExp); // 4jds75hf
    }

    return match ? match[0] : version;
  }

  exports.default = Ember.Helper.helper(appVersion);
});
;define('frontend/helpers/eq', ['exports', 'ember-truth-helpers/helpers/equal'], function (exports, _equal) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _equal.default;
    }
  });
  Object.defineProperty(exports, 'equal', {
    enumerable: true,
    get: function () {
      return _equal.equal;
    }
  });
});
;define('frontend/helpers/gt', ['exports', 'ember-truth-helpers/helpers/gt'], function (exports, _gt) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _gt.default;
    }
  });
  Object.defineProperty(exports, 'gt', {
    enumerable: true,
    get: function () {
      return _gt.gt;
    }
  });
});
;define('frontend/helpers/gte', ['exports', 'ember-truth-helpers/helpers/gte'], function (exports, _gte) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _gte.default;
    }
  });
  Object.defineProperty(exports, 'gte', {
    enumerable: true,
    get: function () {
      return _gte.gte;
    }
  });
});
;define('frontend/helpers/is-array', ['exports', 'ember-truth-helpers/helpers/is-array'], function (exports, _isArray) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _isArray.default;
    }
  });
  Object.defineProperty(exports, 'isArray', {
    enumerable: true,
    get: function () {
      return _isArray.isArray;
    }
  });
});
;define('frontend/helpers/is-empty', ['exports', 'ember-truth-helpers/helpers/is-empty'], function (exports, _isEmpty) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _isEmpty.default;
    }
  });
});
;define('frontend/helpers/is-equal', ['exports', 'ember-truth-helpers/helpers/is-equal'], function (exports, _isEqual) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _isEqual.default;
    }
  });
  Object.defineProperty(exports, 'isEqual', {
    enumerable: true,
    get: function () {
      return _isEqual.isEqual;
    }
  });
});
;define('frontend/helpers/lt', ['exports', 'ember-truth-helpers/helpers/lt'], function (exports, _lt) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _lt.default;
    }
  });
  Object.defineProperty(exports, 'lt', {
    enumerable: true,
    get: function () {
      return _lt.lt;
    }
  });
});
;define('frontend/helpers/lte', ['exports', 'ember-truth-helpers/helpers/lte'], function (exports, _lte) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _lte.default;
    }
  });
  Object.defineProperty(exports, 'lte', {
    enumerable: true,
    get: function () {
      return _lte.lte;
    }
  });
});
;define('frontend/helpers/not-eq', ['exports', 'ember-truth-helpers/helpers/not-equal'], function (exports, _notEqual) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _notEqual.default;
    }
  });
  Object.defineProperty(exports, 'notEq', {
    enumerable: true,
    get: function () {
      return _notEqual.notEq;
    }
  });
});
;define('frontend/helpers/not', ['exports', 'ember-truth-helpers/helpers/not'], function (exports, _not) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _not.default;
    }
  });
  Object.defineProperty(exports, 'not', {
    enumerable: true,
    get: function () {
      return _not.not;
    }
  });
});
;define('frontend/helpers/or', ['exports', 'ember-truth-helpers/helpers/or'], function (exports, _or) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _or.default;
    }
  });
  Object.defineProperty(exports, 'or', {
    enumerable: true,
    get: function () {
      return _or.or;
    }
  });
});
;define('frontend/helpers/pluralize', ['exports', 'ember-inflector/lib/helpers/pluralize'], function (exports, _pluralize) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _pluralize.default;
});
;define('frontend/helpers/singularize', ['exports', 'ember-inflector/lib/helpers/singularize'], function (exports, _singularize) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _singularize.default;
});
;define('frontend/helpers/xor', ['exports', 'ember-truth-helpers/helpers/xor'], function (exports, _xor) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _xor.default;
    }
  });
  Object.defineProperty(exports, 'xor', {
    enumerable: true,
    get: function () {
      return _xor.xor;
    }
  });
});
;define('frontend/initializers/app-version', ['exports', 'ember-cli-app-version/initializer-factory', 'frontend/config/environment'], function (exports, _initializerFactory, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  let name, version;
  if (_environment.default.APP) {
    name = _environment.default.APP.name;
    version = _environment.default.APP.version;
  }

  exports.default = {
    name: 'App Version',
    initialize: (0, _initializerFactory.default)(name, version)
  };
});
;define('frontend/initializers/container-debug-adapter', ['exports', 'ember-resolver/resolvers/classic/container-debug-adapter'], function (exports, _containerDebugAdapter) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'container-debug-adapter',

    initialize() {
      let app = arguments[1] || arguments[0];

      app.register('container-debug-adapter:main', _containerDebugAdapter.default);
      app.inject('container-debug-adapter:main', 'namespace', 'application:main');
    }
  };
});
;define('frontend/initializers/ember-data', ['exports', 'ember-data/setup-container', 'ember-data'], function (exports, _setupContainer) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'ember-data',
    initialize: _setupContainer.default
  };
});
;define('frontend/initializers/export-application-global', ['exports', 'frontend/config/environment'], function (exports, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.initialize = initialize;
  function initialize() {
    var application = arguments[1] || arguments[0];
    if (_environment.default.exportApplicationGlobal !== false) {
      var theGlobal;
      if (typeof window !== 'undefined') {
        theGlobal = window;
      } else if (typeof global !== 'undefined') {
        theGlobal = global;
      } else if (typeof self !== 'undefined') {
        theGlobal = self;
      } else {
        // no reasonable global, just bail
        return;
      }

      var value = _environment.default.exportApplicationGlobal;
      var globalName;

      if (typeof value === 'string') {
        globalName = value;
      } else {
        globalName = Ember.String.classify(_environment.default.modulePrefix);
      }

      if (!theGlobal[globalName]) {
        theGlobal[globalName] = application;

        application.reopen({
          willDestroy: function () {
            this._super.apply(this, arguments);
            delete theGlobal[globalName];
          }
        });
      }
    }
  }

  exports.default = {
    name: 'export-application-global',

    initialize: initialize
  };
});
;define("frontend/instance-initializers/ember-data", ["exports", "ember-data/initialize-store-service"], function (exports, _initializeStoreService) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: "ember-data",
    initialize: _initializeStoreService.default
  };
});
;define('frontend/models/recipes', ['exports', 'ember-data/model', 'ember-data/attr'], function (exports, _model, _attr) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _model.default.extend({
    id: (0, _attr.default)(),
    name: (0, _attr.default)(),
    type: (0, _attr.default)(),
    hardness: (0, _attr.default)(),
    ingredients: (0, _attr.default)(),
    time: (0, _attr.default)(),
    description: (0, _attr.default)(),
    image: (0, _attr.default)(),
    author: (0, _attr.default)(),
    comments: (0, _attr.default)(),
    deleted: (0, _attr.default)()
  });
});
;define('frontend/resolver', ['exports', 'ember-resolver'], function (exports, _emberResolver) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _emberResolver.default;
});
;define('frontend/router', ['exports', 'frontend/config/environment'], function (exports, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  const Router = Ember.Router.extend({
    location: _environment.default.locationType,
    rootURL: _environment.default.rootURL
  });

  Router.map(function () {});

  exports.default = Router;
});
;define('frontend/routes/application', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Route.extend({
    dataApi: Ember.inject.service('data-api'),

    async model() {
      let dataApi = Ember.get(this, "dataApi");

      fetch('http://localhost:8080/recipes', {
        method: 'GET',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(response => response.json()).then(data => {
        data = data.map(item => {
          item.ingredients = item.ingredients.split(",");

          if (item.comments) {
            item.comments = item.comments.split("&");
          } else {
            item.comments = [];
          }

          return item;
        });
        Ember.set(dataApi, 'recipesData', data);
        dataApi.notifyPropertyChange("recipesData");
      });

      fetch('http://localhost:8080/users', {
        method: 'GET',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(response => response.json()).then(data => {
        Ember.set(dataApi, 'usersData', data);
        dataApi.notifyPropertyChange("usersData");
      });
    },

    setupController: function (controller, model) {
      this._super(...arguments);
    }
  });
});
;define('frontend/services/ajax', ['exports', 'ember-ajax/services/ajax'], function (exports, _ajax) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _ajax.default;
    }
  });
});
;define('frontend/services/data-api', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Service.extend({
    notifications: Ember.inject.service('toast'),
    recipesData: [],
    usersData: [],
    whoami: null,

    selectedRecipe: Ember.computed("recipesData", function () {
      let recipes = Ember.get(this, 'recipesData');

      if (recipes && recipes.length) {
        return recipes[0];
      } else {
        return null;
      }
    }),

    updateRecipe(recipe) {
      let self = this;
      let recipesData = Ember.get(this, 'recipesData');
      let oldRecipeIndex = -1;
      let postData = this.postData.bind(this);
      let notifications = Ember.get(this, "notifications");

      if (recipe.id) {
        oldRecipeIndex = recipesData.findIndex(r => {
          return r.id == recipe.id;
        });
      }

      if (oldRecipeIndex >= 0) {
        postData('http://localhost:8080/recipes/', recipe, 'PUT').then(res => {
          let recipesData = Ember.get(self, 'recipesData');

          Ember.set(recipe, "ingredients", recipe.ingredients.split(','));

          if (recipe.comments) {
            Ember.set(recipe, "comments", recipe.comments.split('&'));
          } else {
            recipe.comments = [];
          }

          recipesData.replace(oldRecipeIndex, 1, [recipe]);
          Ember.set(self, 'selectedRecipe', recipe);
          notifications.success("Pomyślnie zaktualizowano przepis");
        });
      } else {
        postData('http://localhost:8080/recipes/', recipe, 'POST').then(res => {
          let recipesData = Ember.get(self, 'recipesData');
          Ember.set(recipe, 'id', res.id);

          Ember.set(recipe, "ingredients", recipe.ingredients.split(','));

          if (recipe.comments) {
            Ember.set(recipe, "comments", recipe.comments.split('&'));
          } else {
            recipe.comments = [];
          }

          recipesData.pushObject(recipe);
          Ember.set(self, 'selectedRecipe', recipe);
          notifications.success("Pomyślnie dodano przepis");
        });
      }
    },

    deleteRecipe(recipe) {
      let self = this;
      let notifications = Ember.get(this, "notifications");
      let postData = this.postData.bind(this);
      postData('http://localhost:8080/recipes/' + recipe.id, recipe, 'DELETE').then(res => {
        let recipesData = Ember.get(self, 'recipesData');
        recipesData.removeObject(recipe);
        Ember.set(self, 'selectedRecipe', recipesData.length ? recipesData[0] : null);
        notifications.success("Pomyślnie usunięto przepis");
      });
    },

    saveUser(user) {
      let self = this;
      let postData = this.postData.bind(this);
      let notifications = Ember.get(this, "notifications");
      postData('http://localhost:8080/users/', user, 'POST').then(res => {
        let usersData = Ember.get(self, 'usersData');
        Ember.set(user, 'id', res.id);

        usersData.pushObject(user);
        notifications.success("Rejestracja przebiegła pomyślnie");
      });
    },

    async postData(url = '', data = {}, requestType = 'POST') {
      const response = await fetch(url, {
        method: requestType,
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      });
      if (requestType == 'DELETE') {
        return await response.text();
      } else {
        return await response.json();
      }
    }

  });
});
;define('frontend/services/toast', ['exports', 'frontend/config/environment', 'ember-toastr/services/toast'], function (exports, _environment, _toast) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  const toastrOptions = {
    closeButton: true,
    debug: false,
    newestOnTop: true,
    progressBar: true,
    positionClass: 'toast-top-right',
    preventDuplicates: true,
    onclick: null,
    showDuration: '300',
    hideDuration: '1000',
    timeOut: '4000',
    extendedTimeOut: '1000',
    showEasing: 'swing',
    hideEasing: 'linear',
    showMethod: 'fadeIn',
    hideMethod: 'fadeOut'
  };
  const config = _environment.default['ember-toastr'] || {
    toastrOptions: toastrOptions
  };

  exports.default = _toast.default.extend({
    defaultToastrOptions: toastrOptions,
    config: config
  });
});
;define("frontend/templates/application", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "hywiYLcx", "block": "{\"symbols\":[\"comment\",\"ingredient\",\"recipe\",\"index\",\"user\",\"recipeType\",\"recipeType\",\"type\",\"user\",\"recipeType\",\"recipeType\",\"type\"],\"statements\":[[7,\"div\"],[11,\"class\",\"main-container\"],[9],[0,\"\\n  \"],[7,\"div\"],[11,\"class\",\"left-column\"],[9],[0,\"\\n    \"],[7,\"div\"],[11,\"class\",\"top-bar\"],[9],[0,\"\\n\"],[4,\"if\",[[23,[\"dataApi\",\"whoami\"]]],null,{\"statements\":[[0,\"        \"],[7,\"button\"],[11,\"class\",\"add-new-recipe\"],[3,\"action\",[[22,0,[]],\"addNewRecipe\"]],[9],[0,\"Dodaj przepis\"],[10],[0,\"\\n        \"],[7,\"button\"],[11,\"class\",\"logout-button add-new-recipe\"],[3,\"action\",[[22,0,[]],\"logout\"]],[9],[0,\"Wyloguj\"],[10],[0,\"\\n        \"],[7,\"div\"],[11,\"class\",\"filter\"],[9],[0,\"\\n          \"],[7,\"span\"],[9],[0,\"Filtrowanie: \"],[10],[0,\"\\n          \"],[7,\"select\"],[11,\"class\",\"add-new-recipe\"],[12,\"onchange\",[27,\"action\",[[22,0,[]],\"pickFilterType\"],[[\"value\"],[\"target.value\"]]]],[9],[0,\"\\n\"],[4,\"each\",[[23,[\"filterTypes\"]]],null,{\"statements\":[[0,\"              \"],[7,\"option\"],[12,\"value\",[22,12,[]]],[9],[1,[22,12,[]],false],[10],[0,\"\\n\"]],\"parameters\":[12]},null],[0,\"          \"],[10],[0,\"\\n\"],[4,\"if\",[[27,\"not-eq\",[[23,[\"filterType\"]],\"Wszystko\"],null]],null,{\"statements\":[[4,\"if\",[[27,\"eq\",[[23,[\"filterType\"]],\"Rodzaj\"],null]],null,{\"statements\":[[0,\"              \"],[7,\"select\"],[11,\"class\",\"add-new-recipe\"],[12,\"onchange\",[27,\"action\",[[22,0,[]],\"pickFilterOption\"],[[\"value\"],[\"target.value\"]]]],[9],[0,\"\\n\"],[4,\"each\",[[23,[\"recipeTypes\"]]],null,{\"statements\":[[0,\"                  \"],[7,\"option\"],[12,\"value\",[22,11,[]]],[9],[1,[22,11,[]],false],[10],[0,\"\\n\"]],\"parameters\":[11]},null],[0,\"              \"],[10],[0,\"\\n\"]],\"parameters\":[]},null],[4,\"if\",[[27,\"eq\",[[23,[\"filterType\"]],\"Trudność\"],null]],null,{\"statements\":[[0,\"              \"],[7,\"select\"],[11,\"class\",\"add-new-recipe\"],[12,\"onchange\",[27,\"action\",[[22,0,[]],\"pickFilterOption\"],[[\"value\"],[\"target.value\"]]]],[9],[0,\"\\n\"],[4,\"each\",[[23,[\"hardnessTypes\"]]],null,{\"statements\":[[0,\"                  \"],[7,\"option\"],[12,\"value\",[22,10,[]]],[9],[1,[22,10,[]],false],[10],[0,\"\\n\"]],\"parameters\":[10]},null],[0,\"              \"],[10],[0,\"\\n\"]],\"parameters\":[]},null],[4,\"if\",[[27,\"eq\",[[23,[\"filterType\"]],\"Autor\"],null]],null,{\"statements\":[[0,\"              \"],[7,\"select\"],[11,\"class\",\"add-new-recipe\"],[12,\"onchange\",[27,\"action\",[[22,0,[]],\"pickFilterOption\"],[[\"value\"],[\"target.value\"]]]],[9],[0,\"\\n\"],[4,\"each\",[[23,[\"dataApi\",\"usersData\"]]],null,{\"statements\":[[0,\"                  \"],[7,\"option\"],[12,\"value\",[22,9,[\"username\"]]],[9],[1,[22,9,[\"username\"]],false],[10],[0,\"\\n\"]],\"parameters\":[9]},null],[0,\"              \"],[10],[0,\"\\n\"]],\"parameters\":[]},null]],\"parameters\":[]},null],[0,\"        \"],[10],[0,\"\\n        Witaj \"],[1,[23,[\"dataApi\",\"whoami\",\"username\"]],false],[0,\"!\\n\\n\"]],\"parameters\":[]},{\"statements\":[[0,\"          \"],[7,\"button\"],[11,\"class\",\"login-button add-new-recipe\"],[3,\"action\",[[22,0,[]],\"loginModal\"]],[9],[0,\"Zaloguj\"],[10],[0,\"\\n          \"],[7,\"button\"],[11,\"class\",\"register-button add-new-recipe\"],[3,\"action\",[[22,0,[]],\"registerModal\"]],[9],[0,\"Rejestracja\"],[10],[0,\"\\n\\n          \"],[7,\"div\"],[11,\"class\",\"filter\"],[9],[0,\"\\n          \"],[7,\"span\"],[9],[0,\"Filtrowanie: \"],[10],[0,\"\\n          \"],[7,\"select\"],[11,\"class\",\"add-new-recipe\"],[12,\"onchange\",[27,\"action\",[[22,0,[]],\"pickFilterType\"],[[\"value\"],[\"target.value\"]]]],[9],[0,\"\\n\"],[4,\"each\",[[23,[\"filterTypes\"]]],null,{\"statements\":[[0,\"              \"],[7,\"option\"],[12,\"value\",[22,8,[]]],[9],[1,[22,8,[]],false],[10],[0,\"\\n\"]],\"parameters\":[8]},null],[0,\"          \"],[10],[0,\"\\n\"],[4,\"if\",[[27,\"not-eq\",[[23,[\"filterType\"]],\"Wszystko\"],null]],null,{\"statements\":[[4,\"if\",[[27,\"eq\",[[23,[\"filterType\"]],\"Rodzaj\"],null]],null,{\"statements\":[[0,\"              \"],[7,\"select\"],[11,\"class\",\"add-new-recipe\"],[12,\"onchange\",[27,\"action\",[[22,0,[]],\"pickFilterOption\"],[[\"value\"],[\"target.value\"]]]],[9],[0,\"\\n\"],[4,\"each\",[[23,[\"recipeTypes\"]]],null,{\"statements\":[[0,\"                  \"],[7,\"option\"],[12,\"value\",[22,7,[]]],[9],[1,[22,7,[]],false],[10],[0,\"\\n\"]],\"parameters\":[7]},null],[0,\"              \"],[10],[0,\"\\n\"]],\"parameters\":[]},null],[4,\"if\",[[27,\"eq\",[[23,[\"filterType\"]],\"Trudność\"],null]],null,{\"statements\":[[0,\"              \"],[7,\"select\"],[11,\"class\",\"add-new-recipe\"],[12,\"onchange\",[27,\"action\",[[22,0,[]],\"pickFilterOption\"],[[\"value\"],[\"target.value\"]]]],[9],[0,\"\\n\"],[4,\"each\",[[23,[\"hardnessTypes\"]]],null,{\"statements\":[[0,\"                  \"],[7,\"option\"],[12,\"value\",[22,6,[]]],[9],[1,[22,6,[]],false],[10],[0,\"\\n\"]],\"parameters\":[6]},null],[0,\"              \"],[10],[0,\"\\n\"]],\"parameters\":[]},null],[4,\"if\",[[27,\"eq\",[[23,[\"filterType\"]],\"Autor\"],null]],null,{\"statements\":[[0,\"              \"],[7,\"select\"],[11,\"class\",\"add-new-recipe\"],[12,\"onchange\",[27,\"action\",[[22,0,[]],\"pickFilterOption\"],[[\"value\"],[\"target.value\"]]]],[9],[0,\"\\n\"],[4,\"each\",[[23,[\"dataApi\",\"usersData\"]]],null,{\"statements\":[[0,\"                  \"],[7,\"option\"],[12,\"value\",[22,5,[\"username\"]]],[9],[1,[22,5,[\"username\"]],false],[10],[0,\"\\n\"]],\"parameters\":[5]},null],[0,\"              \"],[10],[0,\"\\n\"]],\"parameters\":[]},null]],\"parameters\":[]},null],[0,\"        \"],[10],[0,\"\\n\"]],\"parameters\":[]}],[0,\"    \"],[10],[0,\"\\n\"],[4,\"each\",[[23,[\"dataApi\",\"recipesData\"]]],null,{\"statements\":[[4,\"if\",[[27,\"not\",[[23,[\"filterOption\"]]],null]],null,{\"statements\":[[0,\"      \"],[7,\"div\"],[11,\"class\",\"recipe-box\"],[3,\"action\",[[22,0,[]],\"pickRecipe\",[22,3,[]]]],[9],[0,\"\\n        \"],[7,\"div\"],[11,\"class\",\"top\"],[9],[0,\"\\n          \"],[7,\"span\"],[11,\"class\",\"recipe-box-title\"],[9],[0,\" \"],[1,[22,3,[\"name\"]],false],[0,\" \"],[10],[0,\"\\n\\n          \"],[7,\"span\"],[11,\"class\",\"pull-right margin-10\"],[9],[0,\" \"],[1,[22,3,[\"hardness\"]],false],[0,\" \"],[10],[0,\"\\n          \"],[7,\"span\"],[11,\"class\",\"pull-right margin-10\"],[9],[0,\" \"],[1,[22,3,[\"time\"]],false],[0,\" \"],[10],[0,\"\\n          \"],[7,\"span\"],[11,\"class\",\"pull-right margin-10\"],[9],[0,\" \"],[1,[22,3,[\"type\"]],false],[0,\" \"],[10],[0,\"\\n        \"],[10],[0,\"\\n\\n        \"],[7,\"div\"],[11,\"class\",\"bottom\"],[9],[0,\"\\n          \"],[7,\"span\"],[9],[0,\" \"],[1,[22,3,[\"author\"]],false],[0,\" \"],[10],[0,\"\\n          \"],[7,\"span\"],[11,\"class\",\"bottom-description\"],[9],[0,\" \"],[1,[22,3,[\"description\"]],false],[0,\" \"],[10],[0,\"\\n\\n\"],[4,\"if\",[[27,\"eq\",[[23,[\"dataApi\",\"whoami\",\"role\"]],\"admin\"],null]],null,{\"statements\":[[0,\"            \"],[7,\"div\"],[11,\"class\",\"recipe-box-buttons\"],[9],[0,\"\\n              \"],[7,\"button\"],[3,\"action\",[[22,0,[]],\"editRecipe\",[22,3,[]]]],[9],[0,\" Edytuj \"],[10],[0,\"\\n              \"],[7,\"button\"],[3,\"action\",[[22,0,[]],\"deleteRecipe\",[22,3,[]]]],[9],[0,\" Usuń \"],[10],[0,\"\\n            \"],[10],[0,\"\\n\"]],\"parameters\":[]},null],[0,\"        \"],[10],[0,\"\\n      \"],[10],[0,\"\\n\"]],\"parameters\":[]},{\"statements\":[[4,\"if\",[[27,\"and\",[[23,[\"filterOption\"]],[27,\"eq\",[[27,\"get\",[[22,3,[]],[23,[\"filterTypeProperty\"]]],null],[23,[\"filterOption\"]]],null]],null]],null,{\"statements\":[[0,\"          \"],[7,\"div\"],[11,\"class\",\"recipe-box\"],[3,\"action\",[[22,0,[]],\"pickRecipe\",[22,3,[]]]],[9],[0,\"\\n            \"],[7,\"div\"],[11,\"class\",\"top\"],[9],[0,\"\\n              \"],[7,\"span\"],[11,\"class\",\"recipe-box-title\"],[9],[0,\" \"],[1,[22,3,[\"name\"]],false],[0,\" \"],[10],[0,\"\\n\\n              \"],[7,\"span\"],[11,\"class\",\"pull-right margin-10\"],[9],[0,\" \"],[1,[22,3,[\"hardness\"]],false],[0,\" \"],[10],[0,\"\\n              \"],[7,\"span\"],[11,\"class\",\"pull-right margin-10\"],[9],[0,\" \"],[1,[22,3,[\"time\"]],false],[0,\" \"],[10],[0,\"\\n              \"],[7,\"span\"],[11,\"class\",\"pull-right margin-10\"],[9],[0,\" \"],[1,[22,3,[\"type\"]],false],[0,\" \"],[10],[0,\"\\n            \"],[10],[0,\"\\n\\n            \"],[7,\"div\"],[11,\"class\",\"bottom\"],[9],[0,\"\\n              \"],[7,\"span\"],[9],[0,\" \"],[1,[22,3,[\"author\"]],false],[0,\" \"],[10],[0,\"\\n              \"],[7,\"span\"],[11,\"class\",\"bottom-description\"],[9],[0,\" \"],[1,[22,3,[\"description\"]],false],[0,\" \"],[10],[0,\"\\n\\n\"],[4,\"if\",[[27,\"eq\",[[23,[\"dataApi\",\"whoami\",\"role\"]],\"admin\"],null]],null,{\"statements\":[[0,\"                \"],[7,\"div\"],[11,\"class\",\"recipe-box-buttons\"],[9],[0,\"\\n                  \"],[7,\"button\"],[3,\"action\",[[22,0,[]],\"editRecipe\",[22,3,[]]]],[9],[0,\" Edytuj \"],[10],[0,\"\\n                  \"],[7,\"button\"],[3,\"action\",[[22,0,[]],\"deleteRecipe\",[22,3,[]]]],[9],[0,\" Usuń \"],[10],[0,\"\\n                \"],[10],[0,\"\\n\"]],\"parameters\":[]},null],[0,\"            \"],[10],[0,\"\\n          \"],[10],[0,\"\\n\"]],\"parameters\":[]},null]],\"parameters\":[]}]],\"parameters\":[3,4]},null],[0,\"  \"],[10],[0,\"\\n  \"],[7,\"div\"],[11,\"class\",\"right-column\"],[9],[0,\"\\n\"],[4,\"if\",[[23,[\"dataApi\",\"selectedRecipe\"]]],null,{\"statements\":[[0,\"      \"],[7,\"div\"],[11,\"class\",\"header\"],[9],[0,\"\\n        \"],[7,\"span\"],[11,\"class\",\"title\"],[9],[0,\" \"],[1,[23,[\"dataApi\",\"selectedRecipe\",\"name\"]],false],[0,\" \"],[10],[0,\"\\n        \"],[7,\"span\"],[11,\"class\",\"author\"],[9],[0,\" \"],[1,[23,[\"dataApi\",\"selectedRecipe\",\"author\"]],false],[0,\" \"],[10],[0,\"\\n        \"],[7,\"div\"],[11,\"class\",\"header-low\"],[9],[0,\"\\n          \"],[7,\"span\"],[9],[0,\" \"],[1,[23,[\"dataApi\",\"selectedRecipe\",\"time\"]],false],[0,\" \"],[10],[0,\"\\n          \"],[7,\"span\"],[9],[0,\" \"],[1,[23,[\"dataApi\",\"selectedRecipe\",\"type\"]],false],[0,\" \"],[10],[0,\"\\n          \"],[7,\"span\"],[9],[0,\" \"],[1,[23,[\"dataApi\",\"selectedRecipe\",\"hardness\"]],false],[0,\" \"],[10],[0,\"\\n        \"],[10],[0,\"\\n      \"],[10],[0,\"\\n\\n      \"],[7,\"div\"],[11,\"class\",\"image\"],[9],[0,\"\\n        \"],[7,\"img\"],[12,\"src\",[23,[\"dataApi\",\"selectedRecipe\",\"image\"]]],[11,\"alt\",\"Zdjęcie potrawy\"],[9],[10],[0,\"\\n      \"],[10],[0,\"\\n\\n      \"],[7,\"div\"],[11,\"class\",\"ingredients\"],[9],[0,\"\\n        \"],[7,\"span\"],[11,\"class\",\"section-title\"],[9],[0,\" Składniki: \"],[10],[0,\"\\n\"],[4,\"each\",[[23,[\"dataApi\",\"selectedRecipe\",\"ingredients\"]]],null,{\"statements\":[[0,\"          \"],[7,\"span\"],[11,\"class\",\"ingredient\"],[9],[0,\"- \"],[1,[22,2,[]],false],[0,\" \"],[10],[0,\"\\n\"]],\"parameters\":[2]},null],[0,\"      \"],[10],[0,\"\\n\\n      \"],[7,\"div\"],[11,\"class\",\"description\"],[9],[0,\"\\n        \"],[7,\"div\"],[11,\"class\",\"section-title\"],[9],[0,\" Opis: \"],[10],[0,\"\\n        \"],[1,[23,[\"dataApi\",\"selectedRecipe\",\"description\"]],false],[0,\"\\n      \"],[10],[0,\"\\n\\n      \"],[7,\"div\"],[11,\"class\",\"comments\"],[9],[0,\"\\n        \"],[7,\"span\"],[11,\"class\",\"section-title\"],[9],[0,\" Komentarze: \"],[10],[0,\"\\n\"],[4,\"if\",[[23,[\"dataApi\",\"whoami\"]]],null,{\"statements\":[[0,\"          \"],[7,\"div\"],[11,\"class\",\"add-comment\"],[9],[0,\"\\n            \"],[1,[27,\"input\",null,[[\"placeholder\",\"type\",\"value\"],[\"Treść komentarza...\",\"text\",[23,[\"newCommentValue\"]]]]],false],[0,\" \"],[7,\"button\"],[11,\"class\",\"new-comment-button\"],[3,\"action\",[[22,0,[]],\"addNewComment\",[23,[\"dataApi\",\"selectedRecipe\"]]]],[9],[0,\" Dodaj \"],[10],[0,\"\\n          \"],[10],[0,\"\\n\"]],\"parameters\":[]},null],[0,\"        \"],[7,\"div\"],[11,\"class\",\"comments-list\"],[9],[0,\"\\n\"],[4,\"each\",[[23,[\"dataApi\",\"selectedRecipe\",\"comments\"]]],null,{\"statements\":[[0,\"            \"],[7,\"div\"],[11,\"class\",\"comment-box\"],[9],[0,\"\\n              \"],[7,\"div\"],[11,\"class\",\"comment\"],[9],[0,\" \"],[1,[22,1,[]],false],[0,\" \"],[10],[0,\"\\n            \"],[10],[0,\"\\n\"]],\"parameters\":[1]},null],[0,\"        \"],[10],[0,\"\\n      \"],[10],[0,\"\\n\\n\"]],\"parameters\":[]},{\"statements\":[[0,\"        \"],[7,\"div\"],[11,\"class\",\"no-recipe\"],[9],[0,\"\\n          W tym momencie nie ma żadnego przepisu :c\\n        \"],[10],[0,\"\\n\"]],\"parameters\":[]}],[0,\"  \"],[10],[0,\"\\n\"],[4,\"if\",[[23,[\"showNewRecipeModal\"]]],null,{\"statements\":[[0,\"    \"],[1,[27,\"add-new-recipe\",null,[[\"showNewRecipeModal\",\"recipeToEdit\"],[[23,[\"showNewRecipeModal\"]],[23,[\"recipeToEdit\"]]]]],false],[0,\"\\n\"]],\"parameters\":[]},null],[4,\"if\",[[23,[\"showRegisterModal\"]]],null,{\"statements\":[[0,\"    \"],[1,[27,\"register-modal\",null,[[\"showRegisterModal\"],[[23,[\"showRegisterModal\"]]]]],false],[0,\"\\n\"]],\"parameters\":[]},null],[4,\"if\",[[23,[\"showLoginModal\"]]],null,{\"statements\":[[0,\"    \"],[1,[27,\"login-modal\",null,[[\"showLoginModal\"],[[23,[\"showLoginModal\"]]]]],false],[0,\"\\n\"]],\"parameters\":[]},null],[10],[0,\"\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "frontend/templates/application.hbs" } });
});
;define("frontend/templates/components/add-new-recipe", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "fv1PChT7", "block": "{\"symbols\":[\"hardness\",\"type\"],\"statements\":[[7,\"div\"],[11,\"class\",\"new-recipe-modal\"],[9],[0,\"\\n  \"],[7,\"div\"],[11,\"class\",\"new-box\"],[9],[0,\"\\n    \"],[7,\"div\"],[11,\"class\",\"row\"],[9],[0,\"\\n      \"],[7,\"span\"],[11,\"class\",\"first-span\"],[9],[0,\"\\n        Nazwa: \\n      \"],[10],[0,\"\\n      \"],[1,[27,\"input\",null,[[\"type\",\"value\"],[\"text\",[23,[\"name\"]]]]],false],[0,\"\\n    \"],[10],[0,\"\\n    \"],[7,\"div\"],[11,\"class\",\"row\"],[9],[0,\"\\n      \"],[7,\"span\"],[11,\"class\",\"first-span\"],[9],[0,\"\\n        Typ przepisu:\\n      \"],[10],[0,\"\\n      \"],[7,\"span\"],[9],[0,\"\\n        \"],[7,\"select\"],[12,\"onchange\",[27,\"action\",[[22,0,[]],\"changeType\"],[[\"value\"],[\"target.value\"]]]],[9],[0,\"\\n\"],[4,\"each\",[[23,[\"types\"]]],null,{\"statements\":[[0,\"            \"],[7,\"option\"],[12,\"value\",[22,2,[]]],[9],[1,[22,2,[]],false],[10],[0,\"\\n\"]],\"parameters\":[2]},null],[0,\"        \"],[10],[0,\"\\n      \"],[10],[0,\"\\n    \"],[10],[0,\"\\n    \"],[7,\"div\"],[11,\"class\",\"row\"],[9],[0,\"\\n      \"],[7,\"span\"],[11,\"class\",\"first-span\"],[9],[0,\"\\n        Trudność\\n      \"],[10],[0,\"\\n      \"],[7,\"span\"],[9],[0,\"\\n        \"],[7,\"select\"],[12,\"onchange\",[27,\"action\",[[22,0,[]],\"changeHardness\"],[[\"value\"],[\"target.value\"]]]],[9],[0,\"\\n\"],[4,\"each\",[[23,[\"hardnessTypes\"]]],null,{\"statements\":[[0,\"            \"],[7,\"option\"],[12,\"value\",[22,1,[]]],[9],[1,[22,1,[]],false],[10],[0,\"\\n\"]],\"parameters\":[1]},null],[0,\"        \"],[10],[0,\"\\n      \"],[10],[0,\"\\n    \"],[10],[0,\"\\n    \"],[7,\"div\"],[11,\"class\",\"row\"],[9],[0,\"\\n      \"],[7,\"span\"],[11,\"class\",\"first-span\"],[9],[0,\"\\n        Składniki:\\n      \"],[10],[0,\"\\n      \"],[7,\"span\"],[9],[0,\"\\n        \"],[1,[27,\"input\",null,[[\"type\",\"class\",\"value\",\"placeholder\"],[\"text\",\"ingredients-input\",[23,[\"ingredients\"]],\"Składnik 1, Składnik 2, ...\"]]],false],[0,\"\\n      \"],[10],[0,\"\\n    \"],[10],[0,\"\\n    \"],[7,\"div\"],[11,\"class\",\"row\"],[9],[0,\"\\n      \"],[7,\"span\"],[11,\"class\",\"first-span\"],[9],[0,\"\\n        Czas wykonania\\n      \"],[10],[0,\"\\n      \"],[7,\"span\"],[9],[0,\"\\n        \"],[1,[27,\"input\",null,[[\"type\",\"value\",\"placeholder\"],[\"text\",[23,[\"time\"]],\"12:00 min\"]]],false],[0,\"\\n      \"],[10],[0,\"\\n    \"],[10],[0,\"\\n    \"],[7,\"div\"],[11,\"class\",\"row\"],[9],[0,\"\\n      \"],[7,\"span\"],[11,\"class\",\"first-span\"],[9],[0,\"\\n        Link do zdjęcia\\n      \"],[10],[0,\"\\n      \"],[7,\"span\"],[9],[0,\"\\n        \"],[1,[27,\"input\",null,[[\"type\",\"value\"],[\"text\",[23,[\"image\"]]]]],false],[0,\"\\n      \"],[10],[0,\"\\n    \"],[10],[0,\"\\n    \"],[7,\"div\"],[11,\"class\",\"row\"],[9],[0,\"\\n      \"],[7,\"span\"],[11,\"class\",\"first-span textarea-span\"],[9],[0,\"\\n        Opis\\n      \"],[10],[0,\"\\n      \"],[7,\"span\"],[9],[0,\"\\n        \"],[7,\"textarea\"],[11,\"maxlength\",\"254\"],[11,\"class\",\"textarea\"],[11,\"cols\",\"50\"],[11,\"rows\",\"10\"],[9],[10],[0,\"      \"],[10],[0,\"\\n    \"],[10],[0,\"\\n    \"],[7,\"div\"],[11,\"class\",\"buttons\"],[9],[0,\"\\n      \"],[7,\"button\"],[11,\"class\",\"save-button\"],[3,\"action\",[[22,0,[]],\"cancel\"]],[9],[0,\"Anuluj\"],[10],[0,\"\\n      \"],[7,\"button\"],[11,\"class\",\"cancel-button\"],[3,\"action\",[[22,0,[]],\"save\"]],[9],[0,\"Zapisz\"],[10],[0,\"\\n    \"],[10],[0,\"\\n  \"],[10],[0,\"\\n\"],[10]],\"hasEval\":false}", "meta": { "moduleName": "frontend/templates/components/add-new-recipe.hbs" } });
});
;define("frontend/templates/components/login-modal", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "yfktXqso", "block": "{\"symbols\":[],\"statements\":[[7,\"div\"],[11,\"class\",\"new-recipe-modal register-modal\"],[9],[0,\"\\n  \"],[7,\"div\"],[11,\"class\",\"new-box\"],[9],[0,\"\\n    \"],[7,\"div\"],[11,\"class\",\"row\"],[9],[0,\"\\n      \"],[7,\"span\"],[11,\"class\",\"first-span\"],[9],[0,\"\\n        Login: \\n      \"],[10],[0,\"\\n      \"],[1,[27,\"input\",null,[[\"type\",\"value\"],[\"text\",[23,[\"username\"]]]]],false],[0,\"\\n    \"],[10],[0,\"\\n    \"],[7,\"div\"],[11,\"class\",\"row\"],[9],[0,\"\\n      \"],[7,\"span\"],[11,\"class\",\"first-span\"],[9],[0,\"\\n        Hasło:\\n      \"],[10],[0,\"\\n      \"],[7,\"span\"],[9],[0,\"\\n        \"],[1,[27,\"input\",null,[[\"type\",\"value\"],[\"password\",[23,[\"password\"]]]]],false],[0,\"\\n      \"],[10],[0,\"\\n    \"],[10],[0,\"\\n    \"],[7,\"div\"],[11,\"class\",\"buttons\"],[9],[0,\"\\n      \"],[7,\"button\"],[11,\"class\",\"save-button\"],[3,\"action\",[[22,0,[]],\"cancel\"]],[9],[0,\"Anuluj\"],[10],[0,\"\\n      \"],[7,\"button\"],[11,\"class\",\"cancel-button\"],[3,\"action\",[[22,0,[]],\"save\"]],[9],[0,\"Zaloguj\"],[10],[0,\"\\n    \"],[10],[0,\"\\n  \"],[10],[0,\"\\n\"],[10]],\"hasEval\":false}", "meta": { "moduleName": "frontend/templates/components/login-modal.hbs" } });
});
;define("frontend/templates/components/register-modal", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "e6ZVrWn1", "block": "{\"symbols\":[],\"statements\":[[7,\"div\"],[11,\"class\",\"new-recipe-modal register-modal\"],[9],[0,\"\\n  \"],[7,\"div\"],[11,\"class\",\"new-box\"],[9],[0,\"\\n    \"],[7,\"div\"],[11,\"class\",\"row\"],[9],[0,\"\\n      \"],[7,\"span\"],[11,\"class\",\"first-span\"],[9],[0,\"\\n        Login: \\n      \"],[10],[0,\"\\n      \"],[1,[27,\"input\",null,[[\"type\",\"value\"],[\"text\",[23,[\"username\"]]]]],false],[0,\"\\n    \"],[10],[0,\"\\n    \"],[7,\"div\"],[11,\"class\",\"row\"],[9],[0,\"\\n      \"],[7,\"span\"],[11,\"class\",\"first-span\"],[9],[0,\"\\n        Hasło:\\n      \"],[10],[0,\"\\n      \"],[7,\"span\"],[9],[0,\"\\n        \"],[1,[27,\"input\",null,[[\"type\",\"value\"],[\"text\",[23,[\"password\"]]]]],false],[0,\"\\n      \"],[10],[0,\"\\n    \"],[10],[0,\"\\n    \"],[7,\"div\"],[11,\"class\",\"buttons\"],[9],[0,\"\\n      \"],[7,\"button\"],[11,\"class\",\"save-button\"],[3,\"action\",[[22,0,[]],\"cancel\"]],[9],[0,\"Anuluj\"],[10],[0,\"\\n      \"],[7,\"button\"],[11,\"class\",\"cancel-button\"],[3,\"action\",[[22,0,[]],\"save\"]],[9],[0,\"Rejestracja\"],[10],[0,\"\\n    \"],[10],[0,\"\\n  \"],[10],[0,\"\\n\"],[10]],\"hasEval\":false}", "meta": { "moduleName": "frontend/templates/components/register-modal.hbs" } });
});
;

;define('frontend/config/environment', [], function() {
  var prefix = 'frontend';
try {
  var metaName = prefix + '/config/environment';
  var rawConfig = document.querySelector('meta[name="' + metaName + '"]').getAttribute('content');
  var config = JSON.parse(unescape(rawConfig));

  var exports = { 'default': config };

  Object.defineProperty(exports, '__esModule', { value: true });

  return exports;
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

});

;
          if (!runningTests) {
            require("frontend/app")["default"].create({"name":"frontend","version":"0.0.0+5c42de83"});
          }
        
//# sourceMappingURL=frontend.map
