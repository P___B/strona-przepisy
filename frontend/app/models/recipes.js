import Model from 'ember-data/model';
import attr from 'ember-data/attr';

export default Model.extend({
  id: attr(),
  name: attr(),
  type: attr(),
  hardness: attr(),
  ingredients: attr(),
  time: attr(),
  description: attr(),
  image: attr(),
  author: attr(),
  comments: attr(),
  deleted: attr(),
});
