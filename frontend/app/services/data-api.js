import Ember from 'ember';

export default Ember.Service.extend({
  notifications: Ember.inject.service('toast'),
  recipesData: [],
  usersData: [],
  whoami: null,

  selectedRecipe: Ember.computed("recipesData", function() {
    let recipes = Ember.get(this, 'recipesData');

    if(recipes && recipes.length) {
      return recipes[0];
    }
    else {
      return null;
    }
  }),

  updateRecipe(recipe) {
    let self = this;
    let recipesData = Ember.get(this, 'recipesData');
    let oldRecipeIndex = -1;
    let postData = this.postData.bind(this);
    let notifications = Ember.get(this, "notifications");

    if(recipe.id) {
      oldRecipeIndex = recipesData.findIndex(r => {
        return r.id == recipe.id;
      });
    }
    
    if(oldRecipeIndex >= 0) {
      postData('http://localhost:8080/recipes/', recipe, 'PUT').then(res => {
        let recipesData = Ember.get(self, 'recipesData');

        Ember.set(recipe, "ingredients" , recipe.ingredients.split(','));
        
        if(recipe.comments) {
          Ember.set(recipe, "comments" , recipe.comments.split('&'));
        }
        else {
          recipe.comments = [];
        }
        
        recipesData.replace(oldRecipeIndex, 1, [recipe]);
        Ember.set(self, 'selectedRecipe', recipe);
        notifications.success("Pomyślnie zaktualizowano przepis")
      });
    }
    else {
      postData('http://localhost:8080/recipes/', recipe, 'POST').then(res => {
        let recipesData = Ember.get(self, 'recipesData');
        Ember.set(recipe, 'id', res.id);

        Ember.set(recipe, "ingredients" , recipe.ingredients.split(','));
        
        if(recipe.comments) {
          Ember.set(recipe, "comments" , recipe.comments.split('&'));
        }
        else {
          recipe.comments = [];
        }

        recipesData.pushObject(recipe);
        Ember.set(self, 'selectedRecipe', recipe);
        notifications.success("Pomyślnie dodano przepis")
      });
    }
  },

  deleteRecipe(recipe) {
    let self = this;
    let notifications = Ember.get(this, "notifications");
    let postData = this.postData.bind(this);
    postData('http://localhost:8080/recipes/' + recipe.id, recipe, 'DELETE').then(res => {
      let recipesData = Ember.get(self, 'recipesData');
      recipesData.removeObject(recipe);
      Ember.set(self, 'selectedRecipe', recipesData.length ? recipesData[0] : null);
      notifications.success("Pomyślnie usunięto przepis")
    });
  },

  saveUser(user) {
    let self = this;
    let postData = this.postData.bind(this);
    let notifications = Ember.get(this, "notifications");
    postData('http://localhost:8080/users/', user, 'POST').then(res => {
      let usersData = Ember.get(self, 'usersData');
      Ember.set(user, 'id', res.id);

      usersData.pushObject(user);
      notifications.success("Rejestracja przebiegła pomyślnie")
    });
  },

  async postData(url = '', data = {}, requestType = 'POST') {
    const response = await fetch(url, {
      method: requestType,
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    });
    if(requestType == 'DELETE') {
      return await response.text();
    }
    else {
      return await response.json();
    }
  }
  
})
