import Ember from 'ember';

export default Ember.Component.extend({
  dataApi: Ember.inject.service('data-api'),
  notifications: Ember.inject.service('toast'),
  username: "Admin",
  password: "Admin",

  actions: {
    save() {
      let dataApi = Ember.get(this, 'dataApi');
      let username = Ember.get(this, 'username');
      let password = Ember.get(this, 'password');

      if(username.length == 0 || password.length == 0) {
        this.get('notifications').error("Wszystkie pola są wymagane")
      }
      else {
        let usersData = dataApi.get("usersData");

        let user = usersData.find(u => u.username == username);

        if(user && user.password == password) {
          Ember.set(dataApi, 'whoami', user);

          this.toggleProperty('showLoginModal');
          
          this.setProperties({
            username: "",
            password: "",
          })
        }
        else {
          this.get('notifications').error("Nie ma takiego użytkownika lub dane są nieprawidłowe")
        }
        
      }
    },
    cancel() {
      this.toggleProperty('showLoginModal');
      this.setProperties({
        username: "",
        password: "",
      })
    }
  }
});
