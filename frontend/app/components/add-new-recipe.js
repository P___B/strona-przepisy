import Ember from 'ember';

export default Ember.Component.extend({
  dataApi: Ember.inject.service('data-api'),
  notifications: Ember.inject.service('toast'),
  name: "",
  type: "Zupa",
  types: ["Zupa", "Drugie danie", "Śniadanie", "Kolacja"],
  hardness: "Łatwe",
  hardnessTypes: ["Łatwe", "Średnie", "Trudne"],
  ingredients: "",
  time: "",
  description: "",
  image: "",
  author: "",
  recipeToEdit: null,

  recipeToEditObserver: Ember.on('init', Ember.observer('recipeToEdit', function() {
    Ember.run.scheduleOnce('afterRender', this, function() {
      let recipeToEdit = Ember.get(this, 'recipeToEdit');
  
      if(recipeToEdit) {
        let {
          name,
          type,
          hardness,
          ingredients,
          time,
          description,
          image,
          author,
        } = recipeToEdit;
  
        ingredients = ingredients.join(',');
        $('.textarea')[0].value = description;
        
        this.setProperties({
          name,
          type,
          hardness,
          ingredients,
          time,
          description,
          image,
          author,
        });
      }
    })
  })),

  actions: {
    changeType(value) {
      Ember.set(this, "type", value);
    },
    changeHardness(value) {
      Ember.set(this, "hardness", value);
    },
    save() {
      let name = Ember.get(this, 'name');
      let type = Ember.get(this, 'type');
      let hardness = Ember.get(this, 'hardness');
      let ingredients = Ember.get(this, 'ingredients');
      let time = Ember.get(this, 'time');
      let description = $('.textarea')[0].value;
      let image = Ember.get(this, 'image');
      let dataApi = Ember.get(this, 'dataApi');
      let author = dataApi.get("whoami.username");
      let recipeToEdit = Ember.get(this, 'recipeToEdit');

      if(name.length == 0 || type.length == 0 || hardness.length == 0 || ingredients.length == 0 || time.length == 0 || description.length == 0 || image.length == 0 || author.length == 0) {
        this.get('notifications').error("Wszystkie pola są wymagane")
      }
      else {
        let recipe = {
          name,
          type,
          hardness,
          ingredients,
          time,
          description,
          image,
          author,
          comments: recipeToEdit ? recipeToEdit.comments.join('&') : "",
          id: recipeToEdit ? recipeToEdit.id : null,
        }

        dataApi.updateRecipe(recipe);

        this.toggleProperty('showNewRecipeModal');
        
        this.setProperties({
          name: "",
          type: "",
          hardness: "",
          ingredients: "",
          time: "",
          description: "",
          image: "",
          author: "",
        })
      }
    },
    cancel() {
      this.toggleProperty('showNewRecipeModal');
      this.setProperties({
        name: "",
        type: "",
        hardness: "",
        ingredients: "",
        time: "",
        description: "",
        image: "",
        author: "",
      })
    }
  }
});
