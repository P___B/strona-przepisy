import Ember from 'ember';

export default Ember.Component.extend({
  dataApi: Ember.inject.service('data-api'),
  notifications: Ember.inject.service('toast'),
  username: "",
  password: "",
  role: "user",

  actions: {
    save() {
      let dataApi = Ember.get(this, 'dataApi');
      let username = Ember.get(this, 'username');
      let password = Ember.get(this, 'password');
      let role = Ember.get(this, 'role');

      if(username.length == 0 || password.length == 0 || role.length == 0) {
        this.get('notifications').error("Wszystkie pola są wymagane")
      }
      else {
        let user = {
          username,
          password,
          role,
        }

        dataApi.saveUser(user);

        this.toggleProperty('showRegisterModal');
        
        this.setProperties({
          username: "",
          password: "",
        })
      }
    },
    cancel() {
      this.toggleProperty('showRegisterModal');
      this.setProperties({
        username: "",
        password: "",
      })
    }
  }
});
