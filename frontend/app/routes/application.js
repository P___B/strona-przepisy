import Ember from 'ember';
import RSVP from 'rsvp';

export default Ember.Route.extend({
  dataApi: Ember.inject.service('data-api'),

  async model() {
    let dataApi = Ember.get(this, "dataApi");

    fetch('http://localhost:8080/recipes', {
      method: 'GET', 
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
    })
    .then(response => response.json())
    .then(data => {
      data = data.map(item => {
        item.ingredients = item.ingredients.split(",");

        if(item.comments) {
          item.comments = item.comments.split("&")
        }
        else {
          item.comments = [];
        }

        return item;
      })
      Ember.set(dataApi, 'recipesData', data);
      dataApi.notifyPropertyChange("recipesData");
    });

    fetch('http://localhost:8080/users', {
      method: 'GET', 
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
    })
    .then(response => response.json())
    .then(data => {
      Ember.set(dataApi, 'usersData', data);
      dataApi.notifyPropertyChange("usersData");
    });
  },

  setupController: function(controller, model) {
    this._super(...arguments);
  },
})