// Baranowski Patryk Z613
import Ember from 'ember';

export default Ember.Controller.extend({
  showNewRecipeModal: false,
  showLoginModal: false,
  showRegisterModal: false,
  dataApi: Ember.inject.service('data-api'),
  recipeToEdit: null,
  notifications: Ember.inject.service('toast'),
  filterType: "Wszystko",
  filterOption: null,

  filterTypeObserver: Ember.observer('filterType', function() {
    if(this.get("filterType") == "Wszystko") {
      this.set('filterOption', null);
    }
    else if(this.get("filterType") == "Rodzaj") {
      this.set('filterOption', "Zupa");
    }
    else if(this.get("filterType") == "Trudność") {
      this.set('filterOption', 'Łatwe');
    }
    else if(this.get("filterType") == "Autor") {
      let users =  Ember.get(this, 'dataApi.usersData');
      this.set('filterOption', users[0].username);
    }
  }),
  filterTypeProperty: Ember.computed('filterType', function() {
    if(this.get("filterType") == "Rodzaj") {
      return "type";
    }
    else if(this.get("filterType") == "Trudność") {
      return "hardness";
    }
    else if(this.get("filterType") == "Autor") {
      return 'author';
    }
  }),
  filterTypes: [
    "Wszystko",
    "Rodzaj",
    'Trudność',
    'Autor'
  ],
  recipeTypes: ["Zupa", "Drugie danie", "Śniadanie", "Kolacja"],
  hardnessTypes: ["Łatwe", "Średnie", "Trudne"],

  whoamiObserver: Ember.observer("dataApi.whoami", function() {
    // Ember.set(this, 'filterOption', null);
    Ember.set(this, 'filterType', "Wszystko");
    // this.notifyPropertyChange('filterType');
    let dataApi = Ember.get(this, 'dataApi');
    dataApi.notifyPropertyChange('recipesData');
  }),

  actions: {
    logout() {
      Ember.set(this, "dataApi.whoami", null);
    },
    pickRecipe(recipe) {
      Ember.set(this, "dataApi.selectedRecipe", recipe);
    },
    loginModal() {
      this.toggleProperty("showLoginModal");
    },
    registerModal() {
      this.toggleProperty("showRegisterModal");
    },
    addNewRecipe() {
      this.toggleProperty("showNewRecipeModal");
      Ember.set(this, "recipeToEdit", null);
    },
    editRecipe(recipe) {
      this.toggleProperty("showNewRecipeModal");
      Ember.set(this, "recipeToEdit", recipe);
    },
    deleteRecipe(recipe) {
      let dataApi = Ember.get(this, 'dataApi');
      dataApi.deleteRecipe(recipe);
    },
    addNewComment(recipe) {
      if(recipe.comments.length < 5) {
        recipe.comments.pushObject(this.get("newCommentValue"));
        
        Ember.set(recipe, "ingredients" , recipe.ingredients.join(','));
        Ember.set(recipe, "comments" , recipe.comments.join('&'));

        this.get('dataApi').updateRecipe(recipe);

        this.set("newCommentValue", "");
      }
      else {
        this.get('notifications').error("Osiągnięto maksymalną liczbę komentarzy")
      }
    },
    pickFilterType(value) {
      Ember.set(this, "filterType", value);
    },
    pickFilterOption(value) {
      Ember.set(this, "filterOption", value);

      let dataApi = Ember.get(this, 'dataApi');
      dataApi.notifyPropertyChange('recipesData');
    }
  }
})